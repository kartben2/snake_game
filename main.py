from turtle import Screen
from snake import Snake
import time
from food import Food
from scoreboard import ScoreBoard

screen = Screen()
screen.setup(600, 600)
screen.bgcolor("black")
screen.title("My Snack Game")
screen.tracer(0)

snack = Snake()
food = Food()
scoreboard = ScoreBoard()
game_on = True

screen.listen()
screen.onkey(snack.up, "Up")
screen.onkey(snack.down,"Down")
screen.onkey(snack.right,"Right")
screen.onkey(snack.left,"Left")

while game_on:
    screen.update()
    time.sleep(0.1)
    snack.move()

    if snack.head.distance(food) < 15:
        food.refreash()
        snack.extend_segment()
        scoreboard.count_score()

    if snack.head.xcor() > 280 or snack.head.xcor() < -280 or snack.head.ycor() > 280 or snack.head.ycor() < -280:
        scoreboard.reset()
        snack.reset()

    for segment in snack.segment_set[1:]:
        if snack.head.distance(segment) < 10:
            scoreboard.reset()
            snack.reset()


screen.exitonclick()
